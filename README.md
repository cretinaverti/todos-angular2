# Todos App

A simple Todo List application developed with Angular.js (4.2.3).

# Requirements
>>>
- [Docker 17.0.0+](https://docs.docker.com/engine/installation/)
>>>

### For Development
>>>
- [Docker Compose 1.13.0+](https://docs.docker.com/compose/install/)
- [Docker Sync 0.4.6](https://github.com/EugenMayer/docker-sync/wiki)
>>>

## Installation
1. Download or clone this repository:
    ```shell
    git clone https://gitlab.com/cretinaverti/todos-angular2.git
    ```

2. After cloning the project, go into the directory:
    ```shell
    cd todos-angular2/
    ```

3. And launch containers:
    ```shell
    make start-dev
    ```

The application should be launched and accessible through your browser on `localhost[:80]`.

`Crtl + C` on terminal to stop the application's containers.


## Build project
To build project, simply enter:
```shell
make cli-build-project

```

That will generate static files into `dist/` directory.