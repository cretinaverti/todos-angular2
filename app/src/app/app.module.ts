import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';

/* Animation. */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/* Data Service. */
import { TodoDataService } from './todo-data.service';

/* Components. */
import { TodoListHeaderComponent } from './todo-list-header/todo-list-header.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoListItemComponent } from './todo-list-item/todo-list-item.component';
import { TodoListFooterComponent } from './todo-list-footer/todo-list-footer.component';

/* Cookies Handlers. */
import { CookieModule } from 'ngx-cookie';
import { ApiService } from './api.service';

/* Swipe Directive. */
// import { SwipeDirective } from './swipe.directive';

@NgModule({
	declarations: [	
	    AppComponent,
	    TodoListHeaderComponent,
	    TodoListComponent,
	    TodoListItemComponent,
	    TodoListFooterComponent,
// 	    SwipeDirective
	],
	imports: [
    	BrowserModule,
    	BrowserAnimationsModule,
	    FormsModule,
	    CookieModule.forRoot(),
	],
	providers: [TodoDataService, ApiService],
	bootstrap: [AppComponent]
})
export class AppModule { }
