import { Component, Output, Input, EventEmitter, Renderer,
			trigger, state, animate, transition, style } from '@angular/core';
import { Todo } from '../todo';
import 'hammerjs';
import 'hammer-timejs';

@Component({
	selector: 'app-todo-list-item',
	templateUrl: './todo-list-item.component.html',
	styleUrls: ['./todo-list-item.component.css'],
	animations: [
		trigger( 'slideState', [
			state('toRight', style({
				'margin-left': '100px'
			})), 
			state('inactive', style({
			})),
			transition('inactive => active', animate('1s ease-in'))
		])
	]
})
export class TodoListItemComponent {
	public x: number;
	private DIRECTION = { LEFT: 2, RIGHT: 4 };
	
		
	@Input()
	todo: Todo;

	@Output()
	toggleComplete: EventEmitter<Todo>;
	
	@Output()
	remove: EventEmitter<Todo>;
	
	constructor() {
		this.toggleComplete = new EventEmitter();
		this.remove = new EventEmitter();
	}
		
	toggleTodoComplete(todo: Todo) {
		this.toggleComplete.emit(todo);
	}
	
	removeTodo(todo: Todo) {
		this.remove.emit(todo);
	}
	
/*
	onSlide(event: any) {
		event.preventDefault();
		switch (event.direction) {
			case this.DIRECTION.LEFT:
				this.removeTodo(todo);
				break;
			case this.DIRECTION.RIGHT:
				this.toggleTodoComplete(todo);
				break;
		}
		
		this.x = event.x;
	}
*/
	
}