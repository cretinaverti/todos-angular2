import { Injectable } from '@angular/core';
import { Todo } from './todo';
import { CookieService } from 'ngx-cookie';

/**
 * Service charged for managing cookies.
 */
@Injectable()
export class ApiService {
	constructor(private cookieService: CookieService) {}
	
	putTodo(todo: Todo): Todo {
		var cookieName = "todo-" + todo.id.toString();
		var cookieValue = {
			title: todo.title.toString(),
			complete: todo.complete
		};
		this.cookieService.putObject(cookieName, cookieValue);
		return todo;
	}

	deleteTodoById(id: number) {
		this.cookieService.remove("todo-" + id.toString());
	}
	
	updateLastId(lastId: number): number {
		this.cookieService.put("lastId", lastId.toString());
		return this.getLastId();
	}
	
	/**
	 * Get last ID Todo cookie. 
	 * @returns {number} id of of latest Todo or null. 
	 */
	getLastId(): number {
		var lastId: string = '';
		return (lastId = this.cookieService.get("lastId")) ? Number(lastId) : null;
	}
		
	getTodoById(id: number): Todo {
		return new Todo( this.cookieService.getObject("todo-" + id.toString()) );
	}
  	
	getAllTodos(): Todo[] {
		var todos: Todo[] = [];
		var allCookies = this.cookieService.getAll();
		
		todos = this.map(allCookies, function(todo, id) {
			if( id.includes("todo") ) { // Filter cookies.
				var newTodo: Todo = new Todo(JSON.parse(todo));
			
				// Get number part of key.
				newTodo.id = Number(id.split("-", 2)[1]);
				
				newTodo.complete = Boolean(newTodo.complete);
				return newTodo;
			}
		});
		return todos.filter(item => typeof item !== 'undefined'); // Don't include 'lasId' object.
	}

	
	/** 
	 * Helper function for parsing Object and returns array of its attributes.
	 */
	private map(obj, callback) {
		var result = [];
		Object.keys(obj).forEach(function(key) {
			result.push( callback.call(obj, obj[key], key, obj) );
		});
		return result;
	}

}
