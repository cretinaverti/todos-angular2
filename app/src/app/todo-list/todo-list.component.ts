import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Todo } from '../todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent {
	
	/* Allows to bring todos' parent component. */
	@Input()
	todos: Todo[];
	
	@Output()
	toggleComplete: EventEmitter<Todo>;

	@Output()
	remove: EventEmitter<Todo>;

	constructor() {
		this.toggleComplete = new EventEmitter();
		this.remove = new EventEmitter();
	}

	onToggleTodoComplete(todo: Todo) {
		this.toggleComplete.emit(todo);
	}
	
	onRemoveTodo(todo: Todo) {
		this.remove.emit(todo);
	}
	
}
