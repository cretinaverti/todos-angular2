import { Component, Output, EventEmitter } from '@angular/core';
import { Todo } from '../todo';

@Component({
	selector: 'app-todo-list-header',
	templateUrl: './todo-list-header.component.html',
	styleUrls: ['./todo-list-header.component.css']
})
export class TodoListHeaderComponent {
	newTodo: Todo;
	
	/** 
	 * Create new event and pass the new todo as argument.
	 */
	@Output()
	add: EventEmitter<Todo>;
	 
	constructor() {
		this.newTodo = new Todo();
		this.add = new EventEmitter()
	}

	addTodo() {
		this.add.emit(this.newTodo);
		this.newTodo = new Todo();
	}
}
