import { Injectable } from '@angular/core';
import { Todo } from './todo';
import { ApiService } from './api.service';

@Injectable()
export class TodoDataService {
	
	private api: ApiService;

	constructor(api: ApiService) {
		this.api = api;
		if ( !this.api.getLastId() ) { this.api.updateLastId(0); } // If 'lastId' cookie already exist.
	}
	
	addTodo(todo: Todo): Todo {
		if(!todo.id) {
			var lastId = this.api.getLastId();
			todo.id = this.api.updateLastId(++lastId);
		}
		return this.api.putTodo(todo);
	}

	deleteTodoById(id: number) {
		this.api.deleteTodoById(id);
  	}

	toggleTodoComplete(todo: Todo): Todo {
		todo.complete = !todo.complete;
		return this.updateTodo(todo);
	}
  	
  	updateTodo(todo: Todo): Todo {
  		return this.api.putTodo(todo);
	}
		
	getAllTodos(): Todo[] {
		return this.api.getAllTodos();
	}
	
	getTodoById(id: number): Todo {
		return this.api.getTodoById(id);
	}
	
}
