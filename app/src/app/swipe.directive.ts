/*
import { Directive, Input, Output, HostListener, EventEmitter } from '@angular/core';

@Directive({
  selector: '[swipe]'
})
export class SwipeDirective {
	private startX: number;
	
	@Input() dX: number;
	
	@Output() 
	locationChange = new EventEmitter<any>();
	
	constructor() {
		this.startX = 0;
    }
	
	
	@HostListener('swipestart', ['$event']) protected onSlideStart(event) {
		event.preventDefault();
		this.startX = this.dX;
	}
	
	@HostListener('swiperight', ['$event']) protected onSlideRight(event) {
		event.preventDefault();
		this.dX = this.startX + event.deltaX;
		this.locationChange.emit({
			x: this.dX,
			type: event.direction
		}); // Send 'locationChange' event with new dx position.
	}

	@HostListener('swipeleft', ['$event']) protected onSlideLeft(event) {
		event.preventDefault();
		this.dX = this.startX - event.deltaX;
		this.locationChange.emit({
			x: this.dX,
			type: event.direction
		}); // Send 'locationChange' event with new dx position.
	}
}
*/