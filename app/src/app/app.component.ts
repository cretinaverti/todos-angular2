import { Component, EventEmitter, Output } from '@angular/core';
import { Todo } from './todo';
import { TodoDataService } from './todo-data.service';


@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['./app.component.css'],
	providers: []
})
export class AppComponent {
	title: string;
	
	private todoDataService: TodoDataService;

	
	@Output()
	toggleComplete: EventEmitter<Todo>;

	@Output()
	remove: EventEmitter<Todo>;
	
	constructor(todoDataService: TodoDataService) {
		this.title = 'Todos';
		this.todoDataService = todoDataService;
		this.toggleComplete = new EventEmitter();
		this.remove = new EventEmitter();
	}
		
	/** 
	 * Handle event emitted by TodoListHeaderComponent.
	 */
	onAddTodo(todo: Todo) {
		this.todoDataService.addTodo(todo);
	}
	
	onToggleTodoComplete(todo: Todo) {
		this.todoDataService.toggleTodoComplete(todo);
	}
	
	onRemoveTodo(todo: Todo) {
		this.todoDataService.deleteTodoById(todo.id);
	}
	
	get todos(): Todo[] {
		return this.todoDataService.getAllTodos();
	}
}
