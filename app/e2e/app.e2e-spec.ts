import { TodosAngular2Page } from './app.po';

describe('todos-angular2 App', () => {
  let page: TodosAngular2Page;

  beforeEach(() => {
    page = new TodosAngular2Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
