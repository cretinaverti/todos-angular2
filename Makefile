P=$(shell pwd)
CURRENT_DIRECTORY=$(shell printf "%q" "$(P)")

DOCKER_DIR=docker

COMMON_YML=$(DOCKER_DIR)/docker-compose.yml
DEV_SYNC_YML=$(DOCKER_DIR)/docker-sync.yml
DEV_YML=$(DOCKER_DIR)/docker-compose-dev.yml

PREFIX_REPO=registry.gitlab.com/cretinaverti
REPO=$(PREFIX_REPO)/todos-angular2

NODE_CONTAINER=todo-node
APP=todos-angular2
APP_DIR=/app
APP_VERSION=1.0.0

CMD=bash

default:
	@echo "Must call a specific subcommand"
	@exit 1

.clear:
	clear

.start-sync:
	docker-sync start --config=$(DEV_SYNC_YML) || true

.stop-sync:
	docker-sync stop --config=$(DEV_SYNC_YML)

.clean-sync:
	docker-sync clean --config=$(DEV_SYNC_YML)


start-dev: .clear .start-sync
	docker-compose -f $(COMMON_YML) -f $(DEV_YML) up

stop-dev: .clear .stop-sync
	docker-compose -f $(COMMON_YML) -f $(DEV_YML) stop

clean-dev: .clear .clean-sync

cli-$(NODE_CONTAINER): .clear
	docker exec -it $(NODE_CONTAINER) sh -c $(CMD)
	@echo "If commmand contains spaces:"
	@echo "\t\t '\"npm install --save-dev gulp-concat\"'"

cli-init-project: .clear
	docker run -it \
			   --name tmp-angular-init \
			   -v $(CURRENT_DIRECTORY)/app:$(APP_DIR) \
			   $(PREFIX_REPO)/docker-angular:4.2.3 \
			   sh -c 'ng new -sg $(APP) --dir .' && \
	docker stop tmp-angular-init && \
	docker rm tmp-angular-init
	@echo "Temp container tmp-angular-init removed."
	@echo "Project $(APP) created."

cli-build-project: .clear
	docker exec -it $(NODE_CONTAINER) sh -c 'ng build --prod --base-href .' && \
	rm -fR dist \ 
	ln -s app/dist dist
